package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"

	pbHello "grpc-gateway/proto/helloworld"
	pbUser "grpc-gateway/proto/user"

	"database/sql"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

const (
	host     = "localhost"
	user     = "db_user"
	password = "db_pass123"
	dbname   = "db_name"
	dbport   = 81
	port     = ":50051"
)

type server struct {
	conn *sql.DB
	pbUser.UnimplementedUserProfilesServer
	// pbHello.UnimplementedGreeterServer
}

type NullString struct {
	String string
	Valid  bool // Valid is true if String is not NULL
}

func main() {
	// Config to connect PGSQL DB
	config := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, 81, user, password, dbname)
	conn, err := sql.Open("postgres", config)
	if err != nil {
		log.Fatalf("Unable to estabilish connection: %v", err)
	}
	defer conn.Close()

	// Ping to the db connection
	err = conn.Ping()
	if err != nil {
		errors.Wrap(err, "Connection not established, ping didn't work")
	}

	// Prepare dependency function listener
	var user_server *server = &server{}
	user_server.conn = conn
	user_server.Run()
}

// Listener and register gRPC server function
func (server *server) Run() error {
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln("Failed to listen:", err)
	}

	// Create a gRPC server object
	s := grpc.NewServer()
	// Attach the Greeter service to the server
	pbHello.RegisterGreeterServer(s, server)
	pbUser.RegisterUserProfilesServer(s, server)
	// Serve gRPC server
	log.Println("Serving gRPC on 0.0.0.0:8080")
	go func() {
		log.Fatalln(s.Serve(lis))
	}()

	conn, err := grpc.Dial(":8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalln("Failed to dial server:", err)
	}

	gwmux := runtime.NewServeMux()
	// Register Greeter
	pbUser.RegisterUserProfilesHandler(context.Background(), gwmux, conn)
	pbHello.RegisterGreeterHandler(context.Background(), gwmux, conn)

	gwServer := &http.Server{
		Addr:    ":8090",
		Handler: gwmux,
	}

	log.Println("Serving gRPC-Gateway on http://0.0.0.0:8090")
	log.Fatalln(gwServer.ListenAndServe())

	return s.Serve(lis)
}

// API functions to access client
func (*server) SayHello(_ context.Context, in *pbHello.HelloRequest) (*pbHello.HelloReply, error) {
	return &pbHello.HelloReply{Message: in.Name + " world"}, nil
}

// Create User
func (server *server) CreateUserProfile(ctx context.Context, req *pbUser.CreateUserProfileRequest) (*pbUser.UserProfile, error) {
	user_profile := req.UserProfile
	query := `insert into users (name, username) values ($1, $2) returning id, name, username`
	err := server.conn.QueryRow(query, user_profile.Name, user_profile.Username).Scan(&user_profile.Id, &user_profile.Name, &user_profile.Username)
	if err != nil {
		fmt.Println("Error insert data", err)
	}

	return user_profile, nil
}

// Update Profile
func (server *server) UpdateUserProfile(ctx context.Context, req *pbUser.UpdateUserProfileRequest) (*pbUser.UserProfile, error) {
	user_profile := req.UserProfile
	query := `update users set name=$1, username=$2 where id=$3 returning id, name, username`
	err := server.conn.QueryRow(query, user_profile.Name, user_profile.Username, user_profile.Id).Scan(&user_profile.Id, &user_profile.Name, &user_profile.Username)
	if err != nil {
		fmt.Println("Error update data", err)
	}

	return user_profile, nil
}

// Delete Profile
// func (server *server) DeleteUserProfile(ctx context.Context, req *pbUser.DeleteUserProfileRequest) string {
// 	id := req.GetId()
// 	query := `delete from users where id=$1`
// 	err := server.conn.QueryRow(query, id)
// 	if err != nil {
// 		fmt.Println("Error delete", err)
// 	}

// 	return id
// }

// Get User profile
func (server *server) GetUserProfile(ctx context.Context, req *pbUser.GetUserProfileRequest) (*pbUser.UserProfile, error) {
	id := req.GetId()
	var name, username sql.NullString

	query := `select name, username from users where id=$1`
	err := server.conn.QueryRow(query, id).Scan(&name, &username)
	if err != nil {
		fmt.Printf("Error scan: %v", err)
	}

	res := pbUser.UserProfile{
		Name:     name.String,
		Username: username.String,
		Id:       id,
	}

	return &res, nil
}

// Get List users
func (server *server) ListUserProfiles(ctx context.Context, req *pbUser.ListUserProfilesRequest) (*pbUser.ListUserProfilesResponse, error) {
	query := `select id, name, username from users`
	result, err := server.conn.Query(query)

	if err != nil {
		fmt.Println("Error when execute query", err)
	}

	rows := []*pbUser.UserProfile{}

	for result.Next() {
		var id, name, username sql.NullString
		if err = result.Scan(&id, &name, &username); err != nil {
			fmt.Println("Error when scan sql", err)
		}

		u := pbUser.UserProfile{
			Id:       id.String,
			Name:     name.String,
			Username: username.String,
		}

		rows = append(rows, &u)
	}
	res := pbUser.ListUserProfilesResponse{Profiles: rows}

	return &res, nil
}
